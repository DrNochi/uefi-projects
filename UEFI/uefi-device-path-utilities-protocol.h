#pragma once

#include "uefi-base.h"
#include "uefi-device-path-utilities-protocol-def.h"

#define EFI_DEVICE_PATH_UTILITIES_PROTOCOL_GUID \
 { 0x379be4e, 0xd706, 0x437d, { 0xb0, 0x37, 0xed, 0xb8, 0x2f, 0xb7, 0x72, 0xa4 }}

typedef struct _EFI_DEVICE_PATH_UTILITIES_PROTOCOL {
	EFI_DEVICE_PATH_UTILS_GET_DEVICE_PATH_SIZE GetDevicePathSize;
	EFI_DEVICE_PATH_UTILS_DUP_DEVICE_PATH DuplicateDevicePath;
	EFI_DEVICE_PATH_UTILS_APPEND_PATH AppendDevicePath;
	EFI_DEVICE_PATH_UTILS_APPEND_NODE AppendDeviceNode;
	EFI_DEVICE_PATH_UTILS_APPEND_INSTANCE AppendDevicePathInstance;
	EFI_DEVICE_PATH_UTILS_GET_NEXT_INSTANCE GetNextDevicePathInstance;
	EFI_DEVICE_PATH_UTILS_IS_MULTI_INSTANCE IsDevicePathMultiInstance;
	EFI_DEVICE_PATH_UTILS_CREATE_NODE CreateDeviceNode;
} EFI_DEVICE_PATH_UTILITIES_PROTOCOL;