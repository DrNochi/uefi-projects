#include <uefi.h>
#include <uefi-graphics-output-protocol.h>

EFI_HANDLE img;
EFI_SYSTEM_TABLE *sys;
EFI_BOOT_SERVICES *bs;
EFI_RUNTIME_SERVICES *rs;
EFI_SIMPLE_TEXT_INPUT_PROTOCOL *in;
EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL *out;

EFI_GUID graphicsOutputProtocol = EFI_GRAPHICS_OUTPUT_PROTOCOL_GUID;
EFI_GRAPHICS_OUTPUT_PROTOCOL *graphicsOutput;
INT32 screenWidth;
INT32 screenHeight;

EFI_GRAPHICS_OUTPUT_BLT_PIXEL black = { 0, 0, 0 };

struct Rect
{
	INT32 X;
	INT32 Y;
	INT32 Width;
	INT32 Height;
	EFI_GRAPHICS_OUTPUT_BLT_PIXEL Color;
};

void sysout(IN CONST CHAR16 *CONST str)
{
	out->OutputString(out, (CHAR16 *)str);
	out->OutputString(out, (CHAR16 *)u"\r\n");
}

CHAR16 *intToString(UINTN integer)
{
	static CHAR16 array[256];
	array[255] = u'\0';

	int i = 254;
	for (; i >= 0 && integer != 0; --i)
	{
		UINTN c = integer % 10;
		array[i] = u'0' + c;
		integer /= 10;
	}

	return &(array[i + 1]);
}

void clearRect(Rect rect)
{
	graphicsOutput->Blt(
		graphicsOutput,
		&black,
		EfiBltVideoFill,
		0, 0,
		rect.X, rect.Y,
		rect.Width, rect.Height,
		0
	);
}

void drawRect(Rect rect)
{
	graphicsOutput->Blt(
		graphicsOutput,
		&rect.Color,
		EfiBltVideoFill,
		0, 0,
		rect.X, rect.Y,
		rect.Width, rect.Height,
		0
	);
}

void getProtocol(IN EFI_HANDLE handle, IN EFI_GUID *protocol, OUT VOID **interface)
{
	if (bs->HandleProtocol(handle, protocol, interface))
	{
		sysout(u"Graphical output is not supported on this device!\r\nPress any key to continue...");

		UINTN eventIndex;
		bs->WaitForEvent(1, &in->WaitForKey, &eventIndex);

		bs->Exit(img, EFI_UNSUPPORTED, 0, nullptr);
	}
}

INT32 ki(Rect paddle, Rect ball, INT32 ballDx, INT32 ballDy)
{
	INT64 newY = ball.Y;
	if ((paddle.X - ball.X) * ballDx > 0)
	{
		newY += ((paddle.X - ball.X) * ballDy) / ballDx;
	}
	else
	{
		if (ballDx > 0)
		{
			newY += ((screenWidth - ball.X + screenWidth - paddle.X) * ballDy) / ballDx;
		}
		else
		{
			newY += ((ball.X + paddle.X) * ballDy) / ballDx;
		}
	}

	INT32 screenCount = (newY / screenHeight);
	INT32 mod = screenCount % 2;
	if ((mod == 0 && newY >= 0) || (mod != 0 && newY < 0))
	{
		newY = newY - (screenCount * screenHeight);
	}
	else
	{
		newY = screenHeight - (newY - (screenCount * screenHeight));
	}

	if (newY < 0)
	{
		newY = screenHeight - newY;
	}
	else if (newY > screenHeight)
	{
		newY -= screenHeight;
	}

	if (newY > paddle.Y && newY < paddle.Y + paddle.Height)
	{
		return 0;
	}
	else if (newY + ball.Height / 2 > paddle.Y + paddle.Height / 2)
	{
		return 1;
	}
	else
	{
		return -1;
	}
}

EFI_STATUS EFIAPI efi_main(
	IN EFI_HANDLE ImageHandle,
	IN EFI_SYSTEM_TABLE *SystemTable)
{
	img = ImageHandle;
	sys = SystemTable;
	bs = sys->BootServices;
	rs = sys->RuntimeServices;
	in = sys->ConIn;
	out = sys->ConOut;

	bs->SetWatchdogTimer(0, 0, 0, nullptr);
	out->ClearScreen(out);

	sysout(u"Press X any time to exit this game\r\nPress any other key to continue...");

	UINTN eventIndex;
	bs->WaitForEvent(1, &in->WaitForKey, &eventIndex);

	EFI_INPUT_KEY input;
	in->ReadKeyStroke(in, &input);

	if (input.UnicodeChar == u'X')
	{
		return EFI_SUCCESS;
	}

	out->ClearScreen(out);

	getProtocol(sys->ConsoleOutHandle, &graphicsOutputProtocol, (VOID **)&graphicsOutput);
	screenWidth = graphicsOutput->Mode->Info->HorizontalResolution;
	screenHeight = graphicsOutput->Mode->Info->VerticalResolution;

	Rect ball, paddleLeft, paddleRight;
	ball.X = screenWidth / 2 - 5;
	ball.Y = screenHeight / 2 - 5;
	ball.Width = 10;
	ball.Height = 10;
	ball.Color = { 255, 255, 255 };

	paddleLeft.X = 20;
	paddleLeft.Y = screenHeight / 2 - 20;
	paddleLeft.Width = 10;
	paddleLeft.Height = 40;
	paddleLeft.Color = { 191, 191, 255 };

	paddleRight.X = screenWidth - 20 - 10;
	paddleRight.Y = screenHeight / 2 - 20;
	paddleRight.Width = 10;
	paddleRight.Height = 40;
	paddleRight.Color = { 191, 255, 191 };

	EFI_EVENT frameTimer;
	bs->CreateEvent(EVT_TIMER, TPL_NOTIFY, nullptr, nullptr, &frameTimer);
	bs->SetTimer(frameTimer, EFI_TIMER_DELAY::TimerPeriodic, 50000);

	EFI_EVENT events[1] = { /*in->WaitForKey,*/ frameTimer };

	INT32 ballDx = 2;//screenWidth / 300;
	INT32 ballDy = 1;//screenHeight / 300;

	/*EFI_TIME lastTime;
	lastTime.Nanosecond = 0;*/

	EFI_INPUT_KEY lastPressedKey;

	while (true)
	{
		bs->WaitForEvent(1, events, &eventIndex);

		INT32 leftDy = 0;
		INT32 rightDy = 0;

		if (in->ReadKeyStroke(in, &input) == EFI_NOT_READY)
		{
			input = lastPressedKey;
		}
		else
		{
			lastPressedKey = input;
		}

		leftDy = 2 * ki(paddleLeft, ball, ballDx, ballDy);
		rightDy = 2 * ki(paddleRight, ball, ballDx, ballDy);

		switch (input.UnicodeChar)
		{
		case u'w':
			leftDy = -2;
			break;

		case u's':
			leftDy = 2;
			break;

		case u'd':
			leftDy = 0;
			break;

		case u'o':
			rightDy = -2;
			break;

		case u'l':
			rightDy = 2;
			break;

		case u'k':
			rightDy = 0;
			break;

		case u'X':
			return EFI_SUCCESS;
		}

		clearRect(ball);
		clearRect(paddleLeft);
		clearRect(paddleRight);

		paddleLeft.Y += leftDy;

		if (paddleLeft.Y < 0)
		{
			paddleLeft.Y -= leftDy;
		}
		else if (paddleLeft.Y > screenHeight - paddleLeft.Height)
		{
			paddleLeft.Y -= leftDy;
		}

		paddleRight.Y += rightDy;

		if (paddleRight.Y < 0)
		{
			paddleRight.Y -= rightDy;
		}
		else if (paddleRight.Y > screenHeight - paddleRight.Height)
		{
			paddleRight.Y -= rightDy;
		}

		ball.X += ballDx;
		ball.Y += ballDy;

		if (ball.X < 0 || ball.X > screenWidth - ball.Width)
		{
			ballDx = -ballDx;
			ball.X += ballDx;
		}
		else if (ball.Y < 0 || ball.Y > screenHeight - ball.Height)
		{
			ballDy = -ballDy;
			ball.Y += ballDy;
		}
		else if (ball.X > 20 && ball.X < 30 && ball.Y > paddleLeft.Y && ball.Y < paddleLeft.Y + paddleLeft.Height)
		{
			ballDx = -ballDx;

			if (ball.Y - paddleLeft.Y > 10 && ball.Y - paddleLeft.Y < 30)
			{
				ballDy = ballDy < 0 ? -2 : 2;
			}
			else
			{
				ballDy = ballDy < 0 ? -1 : 1;
			}
			
			ball.X = 31;
		}
		else if (ball.X > screenWidth - 40 && ball.X < screenWidth - 30 && ball.Y > paddleRight.Y && ball.Y < paddleRight.Y + paddleRight.Height)
		{
			ballDx = -ballDx;

			if (ball.Y - paddleRight.Y > 10 && ball.Y - paddleRight.Y < 30)
			{
				ballDy = ballDy < 0 ? -2 : 2;
			}
			else
			{
				ballDy = ballDy < 0 ? -1 : 1;
			}

			ball.X = screenWidth - 41;
		}

		drawRect(ball);
		drawRect(paddleLeft);
		drawRect(paddleRight);
	}

	return EFI_SUCCESS;
}